# Copyright (c) 2019, Thomas Larsson
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
# ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
# The views and conclusions contained in the software and documentation are those
# of the authors and should not be interpreted as representing official policies,
# either expressed or implied, of the FreeBSD Project.

import bpy
import os
import json
import numpy as np
from mathutils import Vector
from bpy_extras.io_utils import ImportHelper, ExportHelper
from bpy.props import StringProperty, BoolProperty
from .error import AutoFaceError
from .standalone.common import load_json, save_json, load_config, get_abs_path

#-------------------------------------------------------------
#   Utilities
#-------------------------------------------------------------

def make_object(name, verts, edges, faces, scn):
    me = bpy.data.meshes.new(name)
    me.from_pydata(verts, edges, faces)
    ob = bpy.data.objects.new(name, me)
    scn.objects.link(ob)
    scn.objects.active = ob
    return ob


def zup(verts):
    return [(x,-z,y) for x,y,z in verts]


def zups(verts, scale):
    return [(scale*x,-scale*z,scale*y) for x,y,z in verts]


def is_zero(vec):
    return (vec[0] == 0 and vec[1] == 0 and vec[2] == 0)


def make_vertex_group(ob, name, vnums):
    if name in ob.vertex_groups.keys():
        vgrp = ob.vertex_groups[name]
        ob.vertex_groups.remove(vgrp)
    vgrp = ob.vertex_groups.new(name=name)
    for vn in vnums:
        vgrp.add([vn], 1, 'REPLACE')


def get_verts_in_group(ob, name):
    vgrp = ob.vertex_groups[name]
    vnums = []
    for v in ob.data.vertices:
        for g in v.groups:
            if g.group == vgrp.index:
                vnums.append(v.index)
    return vnums


def make_shapekey(ob, name):
    if not ob.data.shape_keys:
        basic = ob.shape_key_add(name="Basic")
    else:
        basic = ob.data.shape_keys.key_blocks[0]
    if name in ob.data.shape_keys.key_blocks.keys():
        skey = ob.data.shape_keys.key_blocks[name]
        ob.shape_key_remove(skey)
    return ob.shape_key_add(name=name)


def get_pair(context, ordered=True):
    ob1 = context.object
    ob2 = None
    for ob in context.scene.objects:
        if ob.select and ob.type == 'MESH' and ob != ob1:
            ob2 = ob
            break
    if ob2 is None:
        return ob1,ob2
    elif not ordered:
        return ob1,ob2
    elif ob1.type == 'ARMATURE':
        return ob1,ob2
    elif len(ob2.data.vertices) > len(ob1.data.vertices):
        return ob2,ob1
    else:
        return ob1,ob2


def select_verts(ob, vnums):
    for v in ob.data.vertices:
        v.select = False
    for vn in vnums:
        ob.data.vertices[vn].select = True


def get_final_offsets(ob):
    offsets = {}
    for vn,v in enumerate(ob.data.vertices):
        offset = offsets[vn] = Vector((0,0,0))
        for skey in ob.data.shape_keys.key_blocks:
            if skey.value != 0:
                offset += (skey.data[vn].co - v.co) * skey.value
    return offsets

#-------------------------------------------------------------
#   Fingerprints
#-------------------------------------------------------------

FingerPrints = {
    #"19296-38115-18872" : ("gen", "Genesis"),
    #"21556-42599-21098" : ("g2f", "Genesis2"),
    #"17418-34326-17000" : ("g3f", "Genesis3 Female"),
    #"17246-33982-16828" : ("g3m", "Genesis3 Male"),
    "16556-32882-16368" : ("g8f", "Genesis8 Female"),
    "16384-32538-16196" : ("g8m", "Genesis8 Male"),
}

def get_finger_print(ob):
    if ob.type == 'MESH':
        selected = [v for v in ob.data.vertices if v.select]
        return ("%d-%d-%d" % (len(ob.data.vertices), len(ob.data.edges), len(ob.data.polygons)), len(selected))

def get_base_mesh(ob):
    finger, nsel = get_finger_print(ob)
    if finger in FingerPrints.keys():
        return FingerPrints[finger][0]
    else:
        raise AutoFaceError("Does not recognize object:  \n%s    " % ob.name)

def get_base_mesh_name(ob):
    finger, nsel = get_finger_print(ob)
    if finger in FingerPrints.keys():
        return FingerPrints[finger][1]
    else:
        return "Unknown"


class VIEW3D_OT_PrintFinger(bpy.types.Operator):
    bl_idname = "aufc.print_finger"
    bl_label = "Print Finger"

    def execute(self, context):
        bpy.ops.object.mode_set(mode='OBJECT')
        ob = context.object
        print("Finger: %s, N sel: %d" % get_finger_print(ob))
        print("Mesh", get_base_mesh_name(ob))
        return{'FINISHED'}


class Genesis:
    @classmethod
    def poll(self, context):
        ob = context.object
        return (ob and ob.type == 'MESH' and "DazMesh" in ob.keys())

#-------------------------------------------------------------
#   Basel Face Model
#-------------------------------------------------------------

class VIEW3D_OT_BuildBFM(bpy.types.Operator):
    bl_idname = "aufc.build_bfm"
    bl_label = "Build BFM"
    bl_description = "Build BFM model from json file"
    bl_options = {'UNDO'}

    build_object = BoolProperty(default = False)
    build_shapes = BoolProperty(default = False)
    build_exprs = BoolProperty(default = False)

    def execute(self, context):
        from mathutils import Vector
        print("Read BFM file")
        bfm = load_json("./data/bfm.json")

        if self.build_object:
            print("Make object")
            ob =  make_object("BFM", zups(bfm["shapeMU"], 0.001), [], bfm["faces"], context.scene)
            make_vertex_group(ob, "Inner", bfm["innerLandmarkIndex"])
            make_vertex_group(ob, "Outer", bfm["outerLandmarkIndex"])
        else:
            ob = context.object

        if self.build_shapes:
            print("Build shapes")
            base,delta,nmorphs = self.get_delta_shapes(bfm)
            self.add_shapekeys("s", 1, ob, base, delta, nmorphs)

        if self.build_exprs:
            print("Build expressions")
            base,delta,nmorphs = self.get_delta_exprs(bfm)
            self.add_shapekeys("e", 3, ob, base, delta, nmorphs)

        return{'FINISHED'}


    def add_shapekeys(self, prefix, srange, ob, base, delta, nmorphs):
        #delta = self.subtract_center(delta)
        for n in range(nmorphs):
            sname = "%s%02d" % (prefix, n)
            skey = make_shapekey(ob, sname)
            print(skey.name)
            skey.slider_min = -srange
            skey.slider_max = srange
            morph = base + delta[n]
            morph = zups(morph, 0.001)
            for n,co in enumerate(morph):
                skey.data[n].co = co
        print("Done")
        return{'FINISHED'}


    def subtract_center(self, delta):
        vn1,vn2 = [25437, 28275]
        center = (delta[:,vn1,:] + delta[:,vn2,:])/2
        #center = delta[:,vn1,:]
        center = center[:,np.newaxis,:]
        delta = delta - center
        return delta


    def get_delta_shapes(self, bfm):
        base = np.array(bfm["shapeMU"])
        pcs = np.array(bfm["shapePC"])
        evs = np.array(bfm["shapeEV"])
        delta = 10 * pcs * evs[:,np.newaxis,np.newaxis]
        return base, delta, len(evs)


    def get_delta_exprs(self, bfm):
        base = np.array(bfm["shapeMU"])
        pcs = np.array(bfm["expPC"])
        evs = np.array(bfm["expEV"])
        delta = pcs * evs[:,np.newaxis,np.newaxis]
        return base, delta, len(evs)

#-------------------------------------------------------------
#   Save and load facemesh
#-------------------------------------------------------------

def find_match(face, body):
    n = 0
    vb = body.data.vertices[0]
    match = []
    for vf in face.data.vertices:
        while (vb.co - vf.co).length > 1e-4:
            n += 1
            vb = body.data.vertices[n]
        vb.select = True
        match.append(n)
    return match


def find_uvs(face, body, match):
    uvs = {}
    struct = dict([(n,m) for m,n in enumerate(match)])
    print(struct.items())
    uvloop = body.data.uv_layers[0]
    n = 0
    for f in body.data.polygons:
        for i,vn in enumerate(f.vertices):
            if vn in struct.keys():
                uvs[struct[vn]] = uvloop.data[n+i].uv
        n += len(f.vertices)
    uvlist = list(uvs.items())
    uvlist.sort()
    return [list(uv[1]) for uv in uvlist]


class VIEW3D_OT_SaveFace(bpy.types.Operator):
    bl_idname = "aufc.save_facemesh"
    bl_label = "Save Face Mesh"
    bl_description = "Save Face"
    bl_options = {'UNDO'}

    def execute(self, context):
        body,face = get_pair(context)
        match = find_match(face, body)
        struct = {
            "verts" : [list(v.co) for v in face.data.vertices],
            "faces" : [list(f.vertices) for f in face.data.polygons],
            "match" : match,
            "uvs"   : find_uvs(face, body, match),
            "delete" : get_verts_in_group(face, "Delete")
        }
        save_json(struct, "./meshes/%s-facemesh.json" % get_base(body))
        return{'FINISHED'}


class VIEW3D_OT_LoadFace(bpy.types.Operator):
    bl_idname = "aufc.load_facemesh"
    bl_label = "Load Face Mesh"
    bl_description = "Load Face"
    bl_options = {'UNDO'}

    def execute(self, context):
        body = context.object
        base = get_base(body)
        struct = load_json("./meshes/%s-facemesh.json" % base)
        face = make_object("%s-face" % base, struct["verts"], [], struct["faces"], context.scene)
        face["DazMesh"] = body["DazMesh"]
        make_vertex_group(face, "Delete", struct["delete"])
        return{'FINISHED'}

#-------------------------------------------------------------
#
#-------------------------------------------------------------

def get_base(ob):
    if ob["DazMesh"] == "Genesis8-female":
        return "g8f"
    elif ob["DazMesh"] == "Genesis8-male":
        return "g8m"
    else:
        print("Unknown base:", ob["DazMesh"])


class VIEW3D_OT_MakeLandmarks(bpy.types.Operator):
    bl_idname = "aufc.make_landmarks"
    bl_label = "Make Landmarks"
    bl_description = "Create landmark mesh from face mesh"
    bl_options = {'UNDO'}

    def execute(self, context):
        scn = context.scene
        face = context.object
        base = get_base(face)
        struct = load_json("./meshes/%s-landmarks.json" % base)
        interior = [face.data.vertices[vn].co for vn in struct["interior"]]
        boundary = [face.data.vertices[vn].co for vn in struct["boundary"]]
        verts = interior + boundary
        edges = [(vn-1,vn) for vn in range(1,len(interior)) if vn not in struct["starters"]]
        lmk = make_object("%s-landmarks" % base, verts, edges, [], scn)
        lmk.show_x_ray = True
        return{'FINISHED'}

#-------------------------------------------------------------
#
#-------------------------------------------------------------

def get_landmarks(ob, name):
    struct = load_json("./results/%s.json" % name)
    coords = struct["verts"]

    xs = [v.co[0] for v in ob.data.vertices]
    zs = [v.co[2] for v in ob.data.vertices]
    xmin = min(xs)
    xmax = max(xs)
    zmin = min(zs)
    zmax = max(zs)
    lx = (xmax - xmin)/2
    lz = (zmax - zmin)/2
    cx = (xmax + xmin)/2
    cz = (zmax + zmin)/2

    landmarks = []
    for n in range(len(coords)):
        co = ob.data.vertices[n].co.copy()
        co[0] = cx + lx*coords[n][0]
        co[2] = cz + lz*coords[n][1]
        landmarks.append(co)

    return landmarks


class VIEW3D_OT_LoadLandmarks(bpy.types.Operator):
    bl_idname = "aufc.load_landmarks"
    bl_label = "Load Landmarks"
    bl_description = "Load landmarks to face from results file"
    bl_options = {'UNDO'}

    def execute(self, context):
        ob = context.object
        config = load_config(ob=ob)
        landmarks = get_landmarks(ob, config["name"])
        skey = make_shapekey(ob, config["name"])
        for n,co in enumerate(landmarks):
            skey.data[n].co = co
        return{'FINISHED'}

#-------------------------------------------------------------
#   Matching
#-------------------------------------------------------------

def match_meshes(gen, ply):
    gvnums = [v.index for v in gen.data.vertices if v.select]
    glist = [tuple(v.co) for v in gen.data.vertices if v.select]
    plist = [tuple(v.co) for v in ply.data.vertices]
    garr = np.array(glist)
    parr = np.array(plist)

    mindist=np.zeros(len(garr))
    minid=np.zeros(len(garr))

    for i,xy in enumerate(garr):
        dists = np.sum((xy-parr)**2,axis=1)
        mindist[i],minid[i]=dists.min(),dists.argmin()

    return [(gvnums[n],int(id)) for n,id in enumerate(minid)]


def match_coords(clist, ob):
    olist = [tuple(v.co) for v in ob.data.vertices]
    carr = np.array(clist)
    oarr = np.array(olist)

    mindist=np.zeros(len(carr))
    minid=np.zeros(len(carr))

    for i,xy in enumerate(carr):
        dists = np.sum((xy-oarr)**2,axis=1)
        mindist[i],minid[i]=dists.min(),dists.argmin()

    print("Min id", minid)
    print("Min dist", mindist)
    return [int(id) for n,id in enumerate(minid)]


class VIEW3D_OT_MatchMeshes(bpy.types.Operator):
    bl_idname = "aufc.match_meshes"
    bl_label = "Match Meshes"
    bl_description = "Match face and body mesh"
    bl_options = {'UNDO'}

    def execute(self, context):
        body,face = get_pair(context)
        print("Body: %s, Face: %s" % (body, face))
        match = match_meshes(face, body)
        struct = {
            "face" : match,
        }
        save_json(struct, "./meshes/%s-match.json" % get_base(body))
        return{'FINISHED'}


class VIEW3D_OT_ListClosest(bpy.types.Operator):
    bl_idname = "aufc.list_closest"
    bl_label = "List Closest"
    bl_description = "For each selected landmark vertex, print the closest face vertex"
    bl_options = {'UNDO'}

    def execute(self, context):
        face,lmk = get_pair(context)
        match = match_meshes(lmk, face)
        for v in face.data.vertices:
            v.select = False
        for vn,_ in match:
            face.data.vertices[vn].select = True
        print("Match")
        print([vn for _,vn in match])
        return{'FINISHED'}


class VIEW3D_OT_ShowShape(bpy.types.Operator):
    bl_idname = "aufc.show_shapes"
    bl_label = "Show Shapes"
    bl_options = {'UNDO'}

    def execute(self, context):
        from .standalone.bfm import get_bfm, project_bfm

        ob = context.object
        nverts = len(ob.data.vertices)
        model = get_bfm()
        for n in range(5):
            shape = np.zeros(99)
            shape[n] = 10
            S,T = project_bfm(model, shape, None, None, None)
            skey = make_shapekey(ob, "S-%02d" % n)
            for n in range(nverts):
                skey.data[n].co = (S[n,0],S[n,1],S[n,2])
        return{'FINISHED'}


class VIEW3D_OT_AveVertex(bpy.types.Operator):
    bl_idname = "aufc.average_vertex"
    bl_label = "Average Vertex"
    bl_options = {'UNDO'}

    def execute(self, context):
        ob = context.object
        for skey in ob.data.shape_keys.key_blocks:
            sum = Vector((0,0,0))
            for vn in [393, 417, 418, 424]:
                sum += skey.data[vn].co
            co = sum/4
            print(skey, skey.data[419].co, co)
            skey.data[419].co == co
            print("  ", skey.data[419].co)
        return{'FINISHED'}


class VIEW3D_OT_SelectAffected(bpy.types.Operator):
    bl_idname = "aufc.select_affected"
    bl_label = "Select Affected"
    bl_description = "Select all body verts that correspond to face verts"
    bl_options = {'UNDO'}

    def execute(self, context):
        ob = context.object
        struct = load_json("./meshes/%s-match.json" % get_base(ob))
        for v in ob.data.vertices:
            v.select = False
        for _,vn in struct["face"]:
            ob.data.vertices[vn].select = True
        return{'FINISHED'}


class VIEW3D_OT_CopyShapekeys(bpy.types.Operator):
    bl_idname = "aufc.copy_shapekeys"
    bl_label = "Copy Shapekeys"
    bl_description = "Copy shapekeys from face to body"
    bl_options = {'UNDO'}

    def execute(self, context):
        body,face = get_pair(context, ordered=False)
        struct = load_json("./meshes/%s-match.json" % get_base(body))
        offs = {}
        for fvn,gvn in struct["face"]:
            offs[fvn] = face.data.vertices[fvn].co - body.data.vertices[gvn].co
            #print("offs", fvn, gvn, offs[fvn])
        for fskey in face.data.shape_keys.key_blocks:
            if fskey.name == "Basic":
                continue
            gskey = make_shapekey(body, fskey.name)
            gskey.slider_min = fskey.slider_min
            gskey.slider_max = fskey.slider_max
            print(gskey.name)
            for fvn,gvn in struct["face"]:
                gskey.data[gvn].co = fskey.data[fvn].co - offs[fvn]
        print("Shapekeys copied %s > %s" % (face.name, body.name))
        return{'FINISHED'}


class VIEW3D_OT_AdjustFaceParts(bpy.types.Operator, Genesis):
    bl_idname = "aufc.adjust_faceparts"
    bl_label = "Adjust Face Parts"
    bl_options = {'UNDO'}

    def adjust(self, ob, key_verts, verts):
        vn1,vn2 = key_verts
        x1 = ob.data.vertices[vn1].co
        x2 = ob.data.vertices[vn2].co
        for skey in ob.data.shape_keys.key_blocks:
            y1 = skey.data[vn1].co
            y2 = skey.data[vn2].co
            s = (y1-y2).length/(x1-x2).length
            dx = y1 - s*x1
            for vn in verts:
                x = ob.data.vertices[vn].co
                y = s*x + dx
                skey.data[vn].co = y

    def execute(self, context):
        ob = context.object
        struct = load_json("./meshes/%s-faceparts.json" % get_base(ob))
        self.adjust(ob, struct["left eye keys"], struct["left eye"])
        self.adjust(ob, struct["right eye keys"], struct["right eye"])
        self.adjust(ob, struct["upper jaw keys"], struct["upper jaw"])
        self.adjust(ob, struct["lower jaw keys"], struct["lower jaw"])
        print("Face parts adjusted")
        return{'FINISHED'}


class VIEW3D_OT_AdjustBoundary(bpy.types.Operator, Genesis):
    bl_idname = "aufc.adjust_boundary"
    bl_label = "Adjust Boundary"
    bl_options = {'UNDO'}

    def execute(self, context):
        ob = context.object
        struct = load_json("./meshes/%s-faceparts.json" % get_base(ob))
        self.adjust(ob, struct["boundary 1"], 0.25)
        self.adjust(ob, struct["boundary 2"], 0.4)
        self.adjust(ob, struct["boundary 3"], 0.6)
        self.adjust(ob, struct["boundary 4"], 0.8)
        print("Boundary adjusted")
        return{'FINISHED'}


    def adjust(self, ob, verts, factor):
        for skey in ob.data.shape_keys.key_blocks:
            for vn in verts:
                y = skey.data[vn].co
                x = ob.data.vertices[vn].co
                skey.data[vn].co = factor*y + (1-factor)*x


class VIEW3D_OT_LoadFaceParts(bpy.types.Operator, Genesis):
    bl_idname = "aufc.load_faceparts"
    bl_label = "Load Face Parts"
    bl_description = "Load face parts as vertex groups"
    bl_options = {'UNDO'}

    def execute(self, context):
        ob = context.object
        struct = load_json("./meshes/%s-faceparts.json" % get_base(ob))
        for key,vnums in struct.items():
            make_vertex_group(ob, key, vnums)
        print("Face parts loaded")
        return{'FINISHED'}


class LoadParams(ImportHelper):
    filename_ext = ".json"
    filter_glob = StringProperty(default="*.json", options={'HIDDEN'})
    filepath = StringProperty(
        name="File Path",
        maxlen=1024,
        default="")

    @classmethod
    def poll(self, context):
        ob = context.object
        return (ob and ob.type == 'MESH' and ob.data.shape_keys)

    def execute(self, context):
        try:
            self.run(context.object, context.scene)
        except AutoFaceError:
            pass
        return{'FINISHED'}

    def run(self, ob, scn):
        with open(self.filepath) as fp:
            struct = json.load(fp)
        skeys = ob.data.shape_keys.key_blocks
        for skey in skeys:
            skey.slider_min = -10
            skey.slider_max = 10

        nframes = len(struct[self.key])
        print("# frames %d" % nframes)

        for frame in range(nframes):
            params = struct[self.key][frame]
            for n,value in enumerate(params):
                sname = "%s%02d" % (self.prefix, n)
                skey = skeys[sname]
                value = self.factor*value
                skey.value = value
                if nframes > 1:
                    skey.keyframe_insert("value", frame = scn.frame_current + frame)

        name = os.path.splitext(os.path.basename(self.filepath))[0]
        if name[-7:].lower() == "_params":
            name = name[:-7]
        ob.AufcName = name
        ob.AufcImgfile = struct["images"][0]
        print("Params loaded")


    def invoke(self, context, event):
        config = load_config()
        self.properties.filepath = get_abs_path([config["autoface_dir"], "parameters/"], isdir=True)
        context.window_manager.fileselect_add(self)
        return {'RUNNING_MODAL'}


class VIEW3D_OT_LoadShapeParams(bpy.types.Operator, LoadParams):
    bl_idname = "aufc.load_shape_params"
    bl_label = "Load Shape Params"
    bl_description = "Load character parameters from json file"
    bl_options = {'UNDO'}

    prefix = "s"
    key = "shapes"
    factor = 0.1


class VIEW3D_OT_LoadExprParams(bpy.types.Operator, LoadParams):
    bl_idname = "aufc.load_expr_params"
    bl_label = "Load Expression Params"
    bl_description = "Load expression parameters from json file"
    bl_options = {'UNDO'}

    prefix = "e"
    key = "exprs"
    factor = 1


class VIEW3D_OT_ClearParams(bpy.types.Operator):
    bl_idname = "aufc.clear_params"
    bl_label = "Clear Params"
    bl_description = "Clear character parameters"
    bl_options = {'UNDO'}

    @classmethod
    def poll(self, context):
        ob = context.object
        return (ob and ob.type == 'MESH' and ob.data.shape_keys)

    def execute(self, context):
        try:
            ob = context.object
            ob.AufcName = ""
            for skey in ob.data.shape_keys.key_blocks:
                skey.value = 0
            print("Params cleared")
        except AutoFaceError:
            pass
        return{'FINISHED'}


def get_affected(ob):
    base = get_base(ob)
    struct = load_json("./meshes/%s-match.json" % base)
    vnums = [gvn for _fvn,gvn in struct["face"]]
    struct = load_json("./meshes/%s-faceparts.json" % base)
    vnums += struct["left eye"] + struct["right eye"] + struct["upper jaw"] + struct["lower jaw"]
    return vnums


class SaveMorphs:
    def execute(self, context):
        ob = context.object
        verts = ob.data.vertices
        vnums = get_affected(ob)
        struct = {}
        for skey in ob.data.shape_keys.key_blocks:
            if skey.name[0] == self.prefix:
                deltas = [list(self.factor*(skey.data[vn].co - verts[vn].co)) for vn in vnums]
                struct[skey.name] = deltas
        save_json(struct, self.file % get_base(ob))
        return{'FINISHED'}


class VIEW3D_OT_SaveShapes(bpy.types.Operator, SaveMorphs):
    bl_idname = "aufc.save_shapes"
    bl_label = "Save Shapes"
    bl_options = {'UNDO'}

    prefix = "s"
    factor = 0.1
    file = "./data/%s-shapes.json"


class VIEW3D_OT_SaveExprs(bpy.types.Operator, SaveMorphs):
    bl_idname = "aufc.save_exprs"
    bl_label = "Save Expressions"
    bl_options = {'UNDO'}

    prefix = "e"
    factor = 1
    file = "./data/%s-exprs.json"


class LoadMorphs:

    @classmethod
    def poll(self, context):
        ob = context.object
        return (ob and ob.type == 'MESH')

    def execute(self, context):
        try:
            self.run(context.object)
        except AutoFaceError:
            pass
        return{'FINISHED'}

    def run(self, ob):
        verts = ob.data.vertices
        vnums = get_affected(ob)
        struct = load_json(self.file % get_base(ob))
        shapes = list(struct.items())
        shapes.sort()

        for name,deltas in shapes:
            skey = make_shapekey(ob, name)
            for n,delta in enumerate(deltas):
                vn = vnums[n]
                skey.data[vn].co = verts[vn].co + self.factor*Vector(delta)


class VIEW3D_OT_LoadShapes(bpy.types.Operator, LoadMorphs):
    bl_idname = "aufc.load_shapes"
    bl_label = "Load Shapes"
    bl_description = "Load the shape morphs to active mesh"
    bl_options = {'UNDO'}

    file = "./data/%s-shapes.json"
    factor = 10


class VIEW3D_OT_LoadExprs(bpy.types.Operator, LoadMorphs):
    bl_idname = "aufc.load_exprs"
    bl_label = "Load Expressions"
    bl_description = "Load the expression morphs to active mesh"
    bl_options = {'UNDO'}

    file = "./data/%s-exprs.json"
    factor = 1


class VIEW3D_OT_UpdateBones(bpy.types.Operator):
    bl_idname = "aufc.update_bones"
    bl_label = "Update Bones"
    bl_description = "Update bones"
    bl_options = {'UNDO'}

    @classmethod
    def poll(self, context):
        ob = context.object
        return (ob and ob.type == 'ARMATURE')

    def execute(self, context):
        try:
            self.run(context)
        except AutoFaceError:
            pass
        return{'FINISHED'}

    def run(self, context):
        rig, ob = get_pair(context)
        heads = {}
        tails = {}
        for bone in rig.data.bones:
            if is_zero(bone.AufcHead):
                bone.AufcHead = bone.head_local
            if is_zero(bone.AufcTail):
                bone.AufcTail = bone.tail_local
            heads[bone.name] = Vector(bone.AufcHead)
            tails[bone.name] = Vector(bone.AufcTail)
            bone.AufcHOffset = Vector((0,0,0))
            bone.AufcTOffset = Vector((0,0,0))

        vnums = range(len(ob.data.vertices))
        offsets = get_final_offsets(ob)

        base = get_base(ob)
        struct = load_json("./meshes/%s-bones.json" % base)

        face_heads = self.get_offsets(struct["face heads"], offsets)
        face_tails = face_heads

        left_eye_head = self.get_offsets(struct["left eye head"], offsets)
        left_eye_heads = len(struct["left eye"]) * left_eye_head
        left_eye_tails = self.get_offsets(struct["left eye tails"], offsets)

        right_eye_head = self.get_offsets(struct["right eye head"], offsets)
        right_eye_heads = len(struct["right eye"]) * right_eye_head
        right_eye_tails = self.get_offsets(struct["right eye tails"], offsets)

        mouth_heads = self.get_offsets(struct["mouth heads"], offsets)
        mouth_tails = self.get_offsets(struct["mouth tails"], offsets)

        bpy.ops.object.mode_set(mode='EDIT')
        self.update(rig, struct["face"], face_heads, face_tails, heads, tails)
        self.update(rig, struct["left eye"], left_eye_heads, left_eye_tails, heads, tails)
        self.update(rig, struct["right eye"], right_eye_heads, right_eye_tails, heads, tails)
        self.update(rig, struct["mouth"], mouth_heads, mouth_tails, heads, tails)
        bpy.ops.object.mode_set(mode='OBJECT')

        self.set_offsets(rig, struct["face"], face_heads, face_tails)
        self.set_offsets(rig, struct["left eye"], left_eye_heads, left_eye_tails)
        self.set_offsets(rig, struct["right eye"], right_eye_heads, right_eye_tails)
        self.set_offsets(rig, struct["mouth"], mouth_heads, mouth_tails)

        print("Done")


    def get_offsets(self, vnums, offsets):
        noffsets = []
        for vn in vnums:
            if isinstance(vn, int):
                offs = offsets[vn]
            else:
                vn1,vn2 = vn
                offs1 = offsets[vn1]
                offs2 = offsets[vn2]
                offs = (offs1 + offs2)/2
            noffsets.append(offs)
        return noffsets


    def set_offsets(sel, rig, bnames, hoffsets, toffsets):
        for n,bname in enumerate(bnames):
            bone = rig.data.bones[bname]
            bone.AufcHOffset = hoffsets[n]
            bone.AufcTOffset = toffsets[n]


    def update(self, rig, bnames, hoffsets, toffsets, heads, tails):
        for n,bname in enumerate(bnames):
            eb = rig.data.edit_bones[bname]
            eb.head = heads[bname] + hoffsets[n]
            eb.tail = tails[bname] + toffsets[n]


class VIEW3D_OT_ClearBones(bpy.types.Operator):
    bl_idname = "aufc.clear_bones"
    bl_label = "Clear Bones"
    bl_description = "Clear bones"
    bl_options = {'UNDO'}

    @classmethod
    def poll(self, context):
        ob = context.object
        return (ob and ob.type == 'ARMATURE')

    def execute(self, context):
        try:
            self.run(context)
        except AutoFaceError:
            pass
        return{'FINISHED'}

    def run(self, context):
        rig = context.object
        heads = {}
        tails = {}
        for bone in rig.data.bones:
            if is_zero(bone.AufcHead):
                heads[bone.name] = bone.head_local
            else:
                heads[bone.name] = bone.AufcHead
            if is_zero(bone.AufcTail):
                tails[bone.name] = bone.tail_local
            else:
                tails[bone.name] = bone.AufcTail
            bone.AufcHOffset = Vector((0,0,0))
            bone.AufcTOffset = Vector((0,0,0))

        bpy.ops.object.mode_set(mode='EDIT')
        for eb in rig.data.edit_bones:
            eb.head = heads[eb.name]
            eb.tail = tails[eb.name]
        bpy.ops.object.mode_set(mode='OBJECT')


class VIEW3D_OT_MatchBones(bpy.types.Operator):
    bl_idname = "aufc.match_bones"
    bl_label = "Match Bones"
    bl_options = {'UNDO'}

    def execute(self, context):
        rig, ob = get_pair(context)
        base = get_base(ob)
        struct = load_json("./meshes/%s-bones.json" % base, ordered=True)

        heads = [rig.data.bones[bname].head_local for bname in struct["face"]]
        struct["face heads"] = match_coords(heads, ob)

        tails = [rig.data.bones[bname].tail_local for bname in struct["left eye"]]
        struct["left eye tails"] = match_coords(tails, ob)

        tails = [rig.data.bones[bname].tail_local for bname in struct["right eye"]]
        struct["right eye tails"] = match_coords(tails, ob)

        save_json(struct, "./meshes/%s-bones.json" % base)
        return{'FINISHED'}


class VIEW3D_OT_SubtractOffset(bpy.types.Operator):
    bl_idname = "aufc.subtract_offset"
    bl_label = "Subtract Offset"
    bl_description = "Subtract offset from selected vert for all shapekeys"
    bl_options = {'UNDO'}

    @classmethod
    def poll(self, context):
        ob = context.object
        return (ob and ob.type == 'MESH')

    def execute(self, context):
        ob = context.object
        vnums = []
        for v in ob.data.vertices:
            if v.select:
                vnums.append(v.index)
        if not vnums:
            print("No vertices selected")
            return{'FINISHED'}

        basic = ob.data.shape_keys.key_blocks[0]
        sum = Vector((0,0,0))
        for vn in vnums:
            sum += basic.data[vn].co
        co = sum/len(vnums)

        nverts = len(ob.data.vertices)
        for skey in ob.data.shape_keys.key_blocks:
            sum = Vector((0,0,0))
            for vn in vnums:
                sum += skey.data[vn].co
            offset = sum/len(vnums) - co
            print(skey.name, offset)
            for vn in range(nverts):
                skey.data[vn].co -= offset
        return{'FINISHED'}


class Copyer:
    @classmethod
    def poll(self, context):
        ob = context.object
        return (ob and ob.type == 'MESH' and not ob.data.shape_keys)

    def execute(self, context):
        src,trg = get_pair(context)
        tbasic = trg.data.shape_keys.key_blocks[0]
        offsets = [tbasic.data[v.index].co - v.co for v in src.data.vertices]
        for vn,dx in enumerate(offsets):
            trg.data.vertices[vn].co -= dx
            for skey in trg.data.shape_keys.key_blocks[0:self.last]:
                skey.data[vn].co -= dx
        return{'FINISHED'}


class VIEW3D_OT_CopyBasic(bpy.types.Operator, Copyer):
    bl_idname = "aufc.copy_basic"
    bl_label = "Copy Basic"
    bl_description = "Copy vertex and basic coordinates from active to selected"
    bl_options = {'UNDO'}

    last = 1


class VIEW3D_OT_SubtractBasic(bpy.types.Operator, Copyer):
    bl_idname = "aufc.subtract_basic"
    bl_label = "Subtract Basic"
    bl_description = "Subtract vertex and shapekey coordinates from active to selected"
    bl_options = {'UNDO'}

    last = 1000


class VIEW3D_OT_ApplyShapekeys(bpy.types.Operator):
    bl_idname = "aufc.apply_shapekeys"
    bl_label = "Apply Shapekeys"
    bl_description = "Apply all shapekeys"
    bl_options = {'UNDO'}

    @classmethod
    def poll(self, context):
        ob = context.object
        return (ob and ob.type == 'MESH')

    def execute(self, context):
        ob = context.object
        coords = [Vector(v.co) for v in ob.data.vertices]
        offsets = get_final_offsets(ob)
        for skey in ob.data.shape_keys.key_blocks:
            skey.value = 0
            ob.shape_key_remove(skey)
        basic2 = make_shapekey(ob, "Basic2")
        basic = ob.data.shape_keys.key_blocks[0]
        for vn,co in enumerate(coords):
            nco = co + offsets[vn]
            ob.data.vertices[vn].co = nco
            basic.data[vn].co = nco
        return{'FINISHED'}


class TempFile:
    filename_ext = ".txt"
    filter_glob = StringProperty(default="*.txt", options={'HIDDEN'})
    filepath = StringProperty(
        name="File Path",
        maxlen=1024,
        default="")

    @classmethod
    def poll(self, context):
        ob = context.object
        return (ob and ob.type == self.type)

    def invoke(self, context, event):
        context.window_manager.fileselect_add(self)
        return {'RUNNING_MODAL'}


class VIEW3D_OT_ListVnums(bpy.types.Operator, TempFile, ExportHelper):
    bl_idname = "aufc.list_vnums"
    bl_label = "List Selected Vertices"

    type = 'MESH'

    def execute(self, context):
        bpy.ops.object.mode_set(mode='OBJECT')
        ob = context.object
        vnums = [v.index for v in ob.data.vertices if v.select]
        path = os.path.splitext(self.filepath)[0] + ".txt"
        with open(path, "w") as fp:
            fp.write(str(vnums) + "\n")
        return{'FINISHED'}


class VIEW3D_OT_ListBones(bpy.types.Operator, TempFile, ExportHelper):
    bl_idname = "aufc.list_bones"
    bl_label = "List Selected Bones"

    type = 'ARMATURE'

    def execute(self, context):
        bpy.ops.object.mode_set(mode='OBJECT')
        rig = context.object
        bones = [bone.name for bone in rig.data.bones if bone.select]
        path = os.path.splitext(self.filepath)[0] + ".txt"
        with open(path, "w") as fp:
            fp.write(str(bones) + "\n")
        return{'FINISHED'}
