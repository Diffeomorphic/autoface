# Copyright (c) 2019, Thomas Larsson
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
# ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
# The views and conclusions contained in the software and documentation are those
# of the authors and should not be interpreted as representing official policies,
# either expressed or implied, of the FreeBSD Project.

bl_info = {
    'name': 'Auto Face',
    'author': 'Thomas Larsson',
    'version': (0,1),
    "blender": (2, 79, 0),
    'location': "Tools panel",
    'description': "Automatically model and texture the face of a character from a photograph",
    'warning': '',
    'wiki_url': "/diffeomorphic.blogspot.se/p/autoface.html",
    'category': 'Mesh'}

import os
import sys

if "bpy" in locals():
    print("Reloading AutoFace v %d.%d" % bl_info["version"])
    import imp
    imp.reload(error)
    imp.reload(standalone)
    imp.reload(match)
    imp.reload(texture)
    imp.reload(daz)
else:
    print("Loading AutoFace v %d.%d" % bl_info["version"])
    from . import error
    from . import standalone
    from . import match
    from . import texture
    from . import daz

import bpy
from bpy.props import *
from .error import *

#------------------------------------------------------------------------
#    Setup panel
#------------------------------------------------------------------------

class AutoFaceUserPanel(bpy.types.Panel):
    bl_label = "AutoFace"
    bl_space_type = "VIEW_3D"
    bl_region_type = "TOOLS"
    bl_category = "AutoFace"

    def draw(self, context):
        layout = self.layout
        ob = context.object
        scn = context.scene
        layout.operator("aufc.load_shapes")
        layout.operator("aufc.load_exprs")
        layout.separator()
        layout.operator("aufc.load_shape_params")
        layout.operator("aufc.load_expr_params")
        layout.operator("aufc.clear_params")
        layout.separator()
        layout.operator("aufc.load_texture")
        layout.operator("aufc.save_texture")
        layout.separator()
        layout.operator("aufc.update_bones")
        layout.operator("aufc.clear_bones")
        layout.separator()
        layout.operator("aufc.save_daz_files")
        layout.prop(scn, "AufcBinary")
        if ob and ob.type == 'MESH':
            from .match import get_base_mesh_name
            layout.separator()
            layout.prop(ob, "AufcName")
            layout.prop(ob, "AufcImgfile")
            layout.label("Base Mesh: %s" % get_base_mesh_name(ob))


class AutoFaceDevelopmentPanel(bpy.types.Panel):
    bl_label = "AutoFace Development"
    bl_space_type = "VIEW_3D"
    bl_region_type = "TOOLS"
    bl_category = "AutoFace"
    bl_options = {'DEFAULT_CLOSED'}

    def draw(self, context):
        layout = self.layout
        scn = context.scene

        layout.label("Make the facemesh: face and lips materials")
        layout.label("Create Delete group")
        layout.label("Don't forget to collaps U-dims")
        layout.operator("aufc.save_facemesh")
        layout.operator("aufc.load_facemesh")
        layout.label("Delete group and match face and body")
        layout.operator("aufc.match_meshes")
        layout.label("Make landmarks for big face")
        layout.operator("aufc.list_closest")
        layout.operator("aufc.make_landmarks")
        layout.operator("aufc.load_landmarks")

        layout.separator()
        layout.label("Load BFM and shapekeys")
        btn = layout.operator("aufc.build_bfm")
        btn.build_object = True
        btn.build_shapes = True
        btn.build_exprs = True
        layout.operator("aufc.build_bfm", text="Build BFM Shapes").build_shapes = True
        layout.operator("aufc.build_bfm", text="Build BFM Exprs").build_exprs = True

        layout.label("Transfer BFM shapekeys to face")
        layout.label("Transfer shapekeys from face to body")
        layout.operator("aufc.select_affected")
        layout.operator("aufc.copy_shapekeys")
        layout.operator("aufc.load_faceparts")
        layout.operator("aufc.adjust_faceparts")
        layout.operator("aufc.adjust_boundary")
        layout.operator("aufc.save_shapes")
        layout.operator("aufc.save_exprs")

        layout.separator()
        layout.label("Match bones")
        layout.operator("aufc.match_bones")

        layout.separator()
        layout.label("Utilities")
        layout.operator("aufc.apply_shapekeys")
        layout.operator("aufc.subtract_offset")
        layout.operator("aufc.copy_basic")
        layout.operator("aufc.subtract_basic")
        layout.operator("aufc.print_finger")
        layout.operator("aufc.list_vnums")
        layout.operator("aufc.list_bones")

        return

        layout.separator()
        layout.operator("aufc.warp_target")
        layout.operator("aufc.transfer_target")

        layout.operator("aufc.move_to_match")
        layout.separator()
        layout.operator("aufc.import_ply")
        layout.operator("aufc.import_shapekeys")
        layout.operator("aufc.extract_face")
        layout.operator("aufc.print_match")
        layout.operator("aufc.warp_face")

# ---------------------------------------------------------------------
#
# ---------------------------------------------------------------------

def register():
    bpy.types.Object.AufcName = StringProperty(
        name = "Name",
        description = "Active character name",
        default="")

    bpy.types.Object.AufcImgfile = StringProperty(
        name = "Image",
        description = "Active character image file",
        default="")

    bpy.types.Scene.AufcBinary = BoolProperty(
        name = "Compress File",
        description = "Compress the DAZ file with gzip",
        default = False)

    bpy.types.Bone.AufcHead = FloatVectorProperty(size=3, default=(0,0,0))
    bpy.types.Bone.AufcTail = FloatVectorProperty(size=3, default=(0,0,0))
    bpy.types.Bone.AufcHOffset = FloatVectorProperty(size=3, default=(0,0,0))
    bpy.types.Bone.AufcTOffset = FloatVectorProperty(size=3, default=(0,0,0))

    bpy.utils.register_module(__name__)


def unregister():
    try:
        bpy.utils.unregister_module(__name__)
    except:
        pass

if __name__ == "__main__":
    unregister()
    register()

print("AutoFace successfully (re)loaded")
