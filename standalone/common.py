#! /usr/bin/env python

import os
import json
import gzip


def join_paths(paths):
    string = "/".join(paths)
    return string.replace("\\", "/").replace("//", "/")


def get_abs_path(path, isdir=False):
    if isinstance(path, list):
        path = join_paths(path)
    if path[0:2] == "./":
        folder = os.path.dirname(__file__)
        path = folder + "/.." + path[1:]
    path = os.path.abspath(os.path.expanduser(path))
    path = path.replace("\\AppData\\Roaming\\SPB_Data", "")
    if isdir and path[-1] != "/":
        path += "/"
    return path


def get_folder(path):
    path = get_abs_path(path, isdir=True)
    if not os.path.exists(path):
        print("Creating directory '%s'" % path)
        os.makedirs(path)
    return path


def get_input_args():
    import argparse

    parser = argparse.ArgumentParser(description="")
    parser.add_argument("filename", type=str, help="Name of input file.")
    parser.add_argument("basemesh", type=str, help="Base mesh: either g8m or g8f.")
    parser.add_argument("-d", "--dir", type=str, default="", help="Directory to look for images in.")

    args = parser.parse_args()

    config = load_config(imgfile=args.filename, base=args.basemesh)
    if args.dir:
        if args.filename.lower() == "all":
            path = get_abs_path(args.dir, isdir=True)
            name = path.split("/")[-1]
            config = load_config(imgfile=name, base=args.basemesh)
        else:
            path = os.path.join(args.dir, args.filename)
        print("Try 1", path)
        if os.path.exists(path):
            return path, args.basemesh, config

    path = get_abs_path([config["autoface_dir"], "inputs/", args.filename])
    print("Try 2", path)
    if os.path.exists(path):
        return path, args.basemesh, config

    folder = os.path.dirname(__file__)
    path = get_abs_path([folder, "../exprnet/figures/", args.filename])
    print("Try 3", path)
    if os.path.exists(path):
        return path, args.basemesh, config

    print("The input file '%s' was not found.\n" % args.filename)
    parser.print_help()
    return None, None, config


def load_config(imgfile=None, ob=None, base=None):
    path = get_abs_path("./config.txt")
    config = {}
    with open(path) as fp:
        for line in fp:
            words = line.split("=")
            if len(words) == 2:
                key = words[0].strip().lower()
                value = words[1].split('"')
                if len(value) >= 2:
                    config[key] = value[1]
    if imgfile:
        config["imgfile"] = imgfile
        config["name"] = os.path.splitext(imgfile)[0]
    elif ob:
        config["name"] = ob.AufcName
        config["imgfile"] = ob.AufcImgfile

    if base:
        base = base.lower()
    elif ob:
        from ..match import get_base_mesh
        base = get_base_mesh(ob)
    else:
        print("No base mesh found.")
        return config

    if base not in ["g8m", "g8f"]:
        msg = "Basemesh must be either G8M or G8F"
        if ob:
            raise RuntimeError(msg)
        else:
            print(msg)
            return None
    config["base"] = base

    for key,value in list(config.items()):
        if key[0:3] == base:
            config[key[4:]] = value

    return config


def change_base(path, base):
    if base != "g8f":
        return path.replace("g8f", base)
    else:
        return path


def load_json(path, base=None, ordered=None):
    from collections import OrderedDict

    if base:
        path = change_base(path, base)
    path = get_abs_path(path)
    try:
        with gzip.open(path, 'rb') as fp:
            bytes = fp.read()
    except IOError:
        bytes = None

    if bytes:
        string = bytes.decode("utf-8")
        if ordered:
            struct = json.loads(string, object_pairs_hook=OrderedDict)
        else:
            struct = json.loads(string)
    else:
        with open(path, "r") as fp:
            if ordered:
                struct = json.load(fp, object_pairs_hook=OrderedDict)
            else:
                struct = json.load(fp)

    print("Loaded", path)
    return struct


def save_json(struct, path, base=None, use_binary=False):
    if base:
        path = change_base(path, base)
    path = get_abs_path(path)

    folder = os.path.dirname(path)
    if not os.path.exists(folder):
        print("Create", folder)
        os.makedirs(folder)

    string = json.dumps(struct)

    if use_binary:
        bytes = string.encode(encoding="utf-8")
        with gzip.open(path, 'wb') as fp:
            fp.write(bytes)
    else:
        for key in struct.keys():
            string = string.replace('"%s"' % key, '\n    "%s"' % key)
        string = string[:-1] + "\n}\n"
        with open(path, "w") as fp:
            fp.write(string)

    print(path, "saved")
    return struct


def dump_json(struct, path, base=None):
    if base:
        path = change_base(path, base)
    string = json.dumps(struct)
    bytes = string.encode('utf-8')
    folder = os.path.dirname(path)
    if not os.path.exists(folder):
        print("Create", folder)
        os.makedirs(folder)
    with gzip.open(path, 'wb') as fp:
        fp.write(bytes)


def normalize(points):
    xs = [pt[0] for pt in points]
    ys = [pt[1] for pt in points]

    xmin = float(min(xs))
    xmax = float(max(xs))
    ymin = float(min(ys))
    ymax = float(max(ys))
    lx = (xmax - xmin)/2
    ly = (ymax - ymin)/2
    cx = (xmax + xmin)/2
    cy = (ymax + ymin)/2
    print(lx,ly,cx,cy)

    normed = []
    for pt in points:
        x = (pt[0] - cx)/lx
        y = (pt[1] - cy)/ly
        normed.append([x,-y])
    return normed
