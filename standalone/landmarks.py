#! /usr/bin/env python

import sys
import dlib
import os
import json
import common
import shutil


def get_landmarks(img):
    predictor_path = common.get_abs_path("./data/shape_predictor_68_face_landmarks.dat")
    detector = dlib.get_frontal_face_detector()
    predictor = dlib.shape_predictor(predictor_path)
    dets = detector(img, 1)
    print("Number of faces detected: {}".format(len(dets)))
    shapes = []
    for k, d in enumerate(dets):
        print("Detection {}: Left: {} Top: {} Right: {} Bottom: {}".format(
            k, d.left(), d.top(), d.right(), d.bottom()))
        # Get the landmarks/parts for the face in box d.
        shape = predictor(img, d)
        print("Part 0: {}, Part 1: {} ...".format(shape.part(0),
                                                  shape.part(1)))
        shapes.append(shape)
    return shapes, dets


def save_detected_face(det, path, config):
    x1,y1 = det.left(), det.top()
    x2,y2 = det.right(), det.bottom()
    with open(path, "w") as fp:
        fp.write(
            "ID,FILE,FACE_X,FACE_Y,FACE_WIDTH,FACE_HEIGHT\n" +
            "%s,./figures/%s,%d,%d,%d,%d\n" %
                (config["name"], config["imgfile"], x1, y1, x2-x1, y2-y1)
        )


def show_landmarks(img, shape):
    win = dlib.image_window()
    win.clear_overlay()
    win.set_image(img)
    win.add_overlay(shape)
    dlib.hit_enter_to_continue()


def find_landmarks(inpath, basemesh, config):
    img = dlib.load_rgb_image(inpath)
    shapes,dets = get_landmarks(img)
    if len(dets) < 1:
        print("No face detected")
        quit()

    shape = shapes[0]
    points = [(shape.part(n).x, shape.part(n).y) for n in range(shape.num_parts)]
    verts = common.normalize(points)

    mesh = common.load_json("./meshes/%s-facemesh.json" % config["base"])
    landmarks = common.load_json("./meshes/%s-landmarks.json" % config["base"])
    uvs = [mesh["uvs"][vn] for vn in landmarks["interior"]]

    struct = {
        "imgfile" : config["imgfile"],
        "points" : points,
        "verts" : verts,
        "uvs" : uvs,
    }

    results = common.get_folder([config["autoface_dir"], "results/"])
    parameters = common.get_folder([config["autoface_dir"], "parameters/"])
    icons = common.get_folder([config["autoface_dir"], "icons/"])
    figures = common.get_folder([config["autoface_dir"], "figures/"])
    textures = common.get_folder([config["autoface_dir"], "textures/"])

    path = common.get_abs_path([results, config["name"] + ".json"])
    common.save_json(struct, path)
    path = common.get_abs_path([figures, config["name"] + ".csv"])
    save_detected_face(dets[0], path, config)
    path = common.get_abs_path([figures, config["imgfile"]])
    shutil.copyfile(inpath, path)
    return img, shape


if __name__ == "__main__":
    print("dlib", dlib.__version__)
    path, basemesh, config = common.get_input_args()
    if path is None:
        quit()
    print("Use image", path)
    img,shape = find_landmarks(path, basemesh, config)
    show_landmarks(img, shape)

