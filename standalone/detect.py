#! /usr/bin/env python

#from __future__ import division
import os
import json
import common
import shutil
import cv2
import time


def detectFaceOpenCVDnn(net, frame):
    frameOpencvDnn = frame.copy()
    frameHeight = frameOpencvDnn.shape[0]
    frameWidth = frameOpencvDnn.shape[1]
    blob = cv2.dnn.blobFromImage(frameOpencvDnn, 1.0, (300, 300), [104, 117, 123], False, False)

    net.setInput(blob)
    detections = net.forward()
    bboxes = []
    maxconf = 0
    for i in range(detections.shape[2]):
        confidence = detections[0, 0, i, 2]
        if confidence > conf_threshold:
            maxconf = confidence
            x1 = int(detections[0, 0, i, 3] * frameWidth)
            y1 = int(detections[0, 0, i, 4] * frameHeight)
            x2 = int(detections[0, 0, i, 5] * frameWidth)
            y2 = int(detections[0, 0, i, 6] * frameHeight)
            bboxes.append([x1, y1, x2, y2])
            cv2.rectangle(frameOpencvDnn, (x1, y1), (x2, y2), (0, 255, 0), int(round(frameHeight/150)), 8)
    return frameOpencvDnn, bboxes, maxconf


def init_net():
    # OpenCV DNN supports 2 networks.
    # 1. FP16 version of the original caffe implementation ( 5.4 MB )
    # 2. 8 bit Quantized version using Tensorflow ( 2.7 MB )
    DNN = "CAFFE"
    if DNN == "CAFFE":
        modelFile = common.get_abs_path("./data/res10_300x300_ssd_iter_140000_fp16.caffemodel")
        configFile = common.get_abs_path("./data/deploy.prototxt")
        print(os.path.abspath(modelFile))
        print(os.path.abspath(configFile))
        print(os.path.abspath(__file__))
        net = cv2.dnn.readNetFromCaffe(configFile, modelFile)
    else:
        modelFile = common.get_abs_path("models/opencv_face_detector_uint8.pb")
        configFile = common.get_abs_path("models/opencv_face_detector.pbtxt")
        net = cv2.dnn.readNetFromTensorflow(modelFile, configFile)

    conf_threshold = 0.3
    return net, conf_threshold


def loop_frames(inpath, outpath, figures):
    cap = cv2.VideoCapture(inpath)
    hasFrame, frame = cap.read()
    if outpath is not None:
        vid_writer = cv2.VideoWriter(outpath, cv2.VideoWriter_fourcc('M','J','P','G'), 15, (frame.shape[1],frame.shape[0]))

    folder = common.get_folder([figures, config["name"]])
    frame_count = 0
    tt_opencvDnn = 0
    boxes = []
    while(1):
        hasFrame, frame = cap.read()
        if not hasFrame:
            break

        imgname = get_image_name(config, frame_count)
        imgpath = common.get_abs_path([folder, imgname + ".png"])
        cv2.imwrite(imgpath, frame)

        frame_count += 1

        t = time.time()
        outOpencvDnn, bboxes, confidence = detectFaceOpenCVDnn(net, frame)
        tt_opencvDnn += time.time() - t
        fpsOpencvDnn = frame_count / tt_opencvDnn
        label = ("OpenCV DNN ; FPS : %.2f" % fpsOpencvDnn)
        cv2.putText(outOpencvDnn, label, (10,50), cv2.FONT_HERSHEY_SIMPLEX, 1.4, (0, 0, 255), 3, cv2.LINE_AA)
        print(frame_count, confidence, bboxes)
        boxes.append(bboxes[0])

        cv2.imshow("Face Detection Comparison", outOpencvDnn)

        if outpath is not None:
            vid_writer.write(outOpencvDnn)
        if frame_count == 1:
            tt_opencvDnn = 0

        k = cv2.waitKey(10)
        if k == 27:
            break

    cv2.destroyAllWindows()
    if outpath is not None:
        vid_writer.release()
    return boxes


def get_image_name(config, n):
    return ("%s-%03d" % (config["name"], n))


def print_csv(config, figures, boxes):
    string = "ID,FILE,FACE_X,FACE_Y,FACE_WIDTH,FACE_HEIGHT\n"
    for n,box in enumerate(boxes):
        x1,y1,x2,y2 = box
        imgname = get_image_name(config, n)
        imgpath = ("./figures/%s/%s.png" % (config["name"], imgname))
        string += ("%s,%s,%d,%d,%d,%d\n" % (imgname, imgpath, x1, y1, x2-x1, y2-y1))
    csvpath = common.get_abs_path([figures, config["name"] + ".csv"])
    with open(csvpath, "w") as fp:
        fp.write(string)


if __name__ == "__main__" :
    inpath, basemesh, config = common.get_input_args()
    if inpath is None:
        quit()
    figures = common.get_folder([config["autoface_dir"], "figures/"])
    outpath = common.get_abs_path([config["autoface_dir"], ('output-dnn-%s.avi' % config["name"])])

    net, conf_threshold = init_net()
    boxes = loop_frames(inpath, None, figures)
    print_csv(config, figures, boxes)

