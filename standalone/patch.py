#! /usr/bin/env python
#
#   This code is based on the following tutorial:
#
#   https://www.learnopencv.com/face-swap-using-opencv-c-python/
#

import os
import json
import numpy as np
import cv2
import common

# Apply affine transform calculated using srcTri and dstTri to src and
# output an image of size.
def apply_affine_transform(src, srcTri, dstTri, size) :

    # Given a pair of triangles, find the affine transform.
    warpMat = cv2.getAffineTransform( np.float32(srcTri), np.float32(dstTri) )

    # Apply the Affine Transform just found to the src image
    dst = cv2.warpAffine( src, warpMat, (size[0], size[1]), None, flags=cv2.INTER_LINEAR, borderMode=cv2.BORDER_REFLECT_101 )

    return dst


# Check if a point is inside a rectangle
def rect_contains(rect, point) :
    if point[0] < rect[0] :
        return False
    elif point[1] < rect[1] :
        return False
    elif point[0] > rect[0] + rect[2] :
        return False
    elif point[1] > rect[1] + rect[3] :
        return False
    return True


#calculate delanauy triangle
def make_delaunay(img, points):
    size = img.shape
    rect = (0, 0, size[1], size[0])
    subdiv = cv2.Subdiv2D(rect);
    for p in points:
        subdiv.insert(p)
    triangles = subdiv.getTriangleList();

    delaunay = []
    for t in triangles:
        pt = []
        pt.append((t[0], t[1]))
        pt.append((t[2], t[3]))
        pt.append((t[4], t[5]))

        pt1 = (t[0], t[1])
        pt2 = (t[2], t[3])
        pt3 = (t[4], t[5])

        if rect_contains(rect, pt1) and rect_contains(rect, pt2) and rect_contains(rect, pt3):
            ind = []
            #Get face-points (from 68 face detector) by coordinates
            for j in range(0, 3):
                for k in range(0, len(points)):
                    if(abs(pt[j][0] - points[k][0]) < 1.0 and abs(pt[j][1] - points[k][1]) < 1.0):
                        ind.append(k)
            # Three points form a triangle. Triangle array corresponds to the file tri.txt in FaceMorph
            if len(ind) == 3:
                delaunay.append((ind[0], ind[1], ind[2]))

    return delaunay


# Warps and alpha blends triangular regions from img1 and img2 to img
def warp_triangle(img1, img2, t1, t2) :

    # Find bounding rectangle for each triangle
    r1 = cv2.boundingRect(np.float32([t1]))
    r2 = cv2.boundingRect(np.float32([t2]))

    # Offset points by left top corner of the respective rectangles
    t1Rect = []
    t2Rect = []
    t2RectInt = []

    for i in range(0, 3):
        t1Rect.append(((t1[i][0] - r1[0]),(t1[i][1] - r1[1])))
        t2Rect.append(((t2[i][0] - r2[0]),(t2[i][1] - r2[1])))
        t2RectInt.append(((t2[i][0] - r2[0]),(t2[i][1] - r2[1])))

    # Get mask by filling triangle
    mask = np.zeros((r2[3], r2[2], 3), dtype = np.float32)
    cv2.fillConvexPoly(mask, np.int32(t2RectInt), (1.0, 1.0, 1.0), 16, 0);

    # Apply warpImage to small rectangular patches
    img1Rect = img1[r1[1]:r1[1] + r1[3], r1[0]:r1[0] + r1[2]]
    #img2Rect = np.zeros((r2[3], r2[2]), dtype = img1Rect.dtype)

    size = (r2[2], r2[3])

    img2Rect = apply_affine_transform(img1Rect, t1Rect, t2Rect, size)

    img2Rect = img2Rect * mask

    # Copy triangular region of the rectangular patch to the output image
    img2[r2[1]:r2[1]+r2[3], r2[0]:r2[0]+r2[2]] = img2[r2[1]:r2[1]+r2[3], r2[0]:r2[0]+r2[2]] * ( (1.0, 1.0, 1.0) - mask )

    img2[r2[1]:r2[1]+r2[3], r2[0]:r2[0]+r2[2]] = img2[r2[1]:r2[1]+r2[3], r2[0]:r2[0]+r2[2]] + img2Rect


def localize_triangle(triangle, points):
    i,j,k = triangle
    return (points[i], points[j], points[k])


def draw_triangles(triangles, img, points):
    CV_AA = 16
    color = (0, 0, 0)
    for t in triangles:
        pt1,pt2,pt3 = localize_triangle(t, points)
        cv2.line(img, pt1, pt2, color, 1, CV_AA, 0)
        cv2.line(img, pt2, pt3, color, 1, CV_AA, 0)
        cv2.line(img, pt3, pt1, color, 1, CV_AA, 0)


def patch_diffuse(imgpath, basemesh, config):
    path = common.get_abs_path([config["autoface_dir"], "results/", config["name"] + ".json"])
    if not os.path.exists(path):
        print("Input file '%s' does not exist" % path)
        quit()
    struct = common.load_json(path)

    ext = os.path.splitext(imgpath)[1]
    img1 = cv2.imread(imgpath);
    if img1 is None:
        print("Cannot open '%s'" % imgpath)
        quit()
    points1 = [tuple(p) for p in struct["points"]]

    # Create and save icon
    if img1.shape[0] > img1.shape[1]:
        rows = 128
        cols = int(128*img1.shape[1]/img1.shape[0])
    else:
        cols = 128
        rows = int(128*img1.shape[0]/img1.shape[1])
    icon = cv2.resize(img1, (cols,rows), interpolation=cv2.INTER_AREA)
    icons = common.get_folder([config["autoface_dir"], "icons/"])
    path = common.get_abs_path([icons, config["name"] + ".png"])
    cv2.imwrite(path, icon)

    # Back on track
    diffuse = common.get_abs_path([config["daz_base_dir"], config["tex_dir"], config["diffuse_texture"]])
    img2 = cv2.imread(diffuse);
    if img2 is None:
        print("Cannot open '%s'" % diffuse)
        return
    sx = img2.shape[0]
    sy = img2.shape[1]
    points2 = [(int(round(sx*(uv[0]))), int(round(sy*(1-uv[1])))) for uv in struct["uvs"]]

    img1Warped = np.copy(img2);

    triangles = make_delaunay(img1, points1)
    if len(triangles) == 0:
        quit()

    # Apply affine transformation to Delaunay triangles
    for t in triangles:
        t1 = localize_triangle(t, points1)
        t2 = localize_triangle(t, points2)
        warp_triangle(img1, img1Warped, t1, t2)

    # Calculate mask
    hull2 = []
    hullIndex = cv2.convexHull(np.array(points2), returnPoints = False)

    for i in range(0, len(hullIndex)):
        hull2.append(points2[int(hullIndex[i])])

    hull8U = []
    for i in range(0, len(hull2)):
        hull8U.append((hull2[i][0], hull2[i][1]))

    mask = np.zeros(img2.shape, dtype = img2.dtype)
    cv2.fillConvexPoly(mask, np.int32(hull8U), (255, 255, 255))

    # Change color
    img1Masked = cv2.multiply(np.float32(mask), np.float32(img1Warped))
    img1Masked = cv2.cvtColor(img1Masked, cv2.COLOR_BGR2HSV)
    img2Masked = cv2.multiply(np.float32(mask), np.float32(img2))
    img2Masked = cv2.cvtColor(img2Masked, cv2.COLOR_BGR2HSV)
    img1HSV = cv2.cvtColor(np.float32(img1Warped),cv2.COLOR_BGR2HSV)
    for i in [1]:
        sum1 = np.sum(img1Masked[:,:,i])
        sum2 = np.sum(img2Masked[:,:,i])
        img1HSV[:,:,i] *= sum2/sum1
    img1Warped = cv2.cvtColor(img1HSV,cv2.COLOR_HSV2BGR)
    #return np.uint8(img1Warped)

    # Clone seamlessly.
    r = cv2.boundingRect(np.float32([hull2]))
    center = ((r[0]+int(r[2]/2), r[1]+int(r[3]/2)))
    output = cv2.seamlessClone(np.uint8(img1Warped), img2, mask, center, cv2.NORMAL_CLONE)

    # Save image
    ext = os.path.splitext(diffuse)[1]
    folder = common.get_folder([config["autoface_dir"], "textures/"])
    outpath = common.join_paths([folder, config["name"] + "_diffuse.png"])
    cv2.imwrite(outpath, output)

    print("'%s' written" % outpath)
    return output


if __name__ == '__main__' :
    # Make sure OpenCV is version 3.0 or above
    (major_ver, minor_ver, subminor_ver) = (cv2.__version__).split('.')

    if int(major_ver) < 3 :
        print('ERROR: Script needs OpenCV 3.0 or higher')
        quit()

    path, basemesh, config = common.get_input_args()
    if path is None:
        quit()
    print("Use image", path)

    output = patch_diffuse(path, basemesh, config)

    if output is not None:
        x,y = (int(output.shape[1]/8),int(output.shape[0]/8))
        small = cv2.resize(output,(x,y))
        cv2.imshow("Face Swapped", small)
        cv2.waitKey(0)
        cv2.destroyAllWindows()

