#! /usr/bin/env python

import numpy as np
import json
import os
import sys
import common


def get_bfm():
    ## Modifed Basel Face Model
    from scipy.io import loadmat
    config = common.load_config()
    model = loadmat(config["bfm_path"], squeeze_me=True, struct_as_record=False)
    model = model["BFM"]
    print('> Loaded the Basel Face Model to write the 3D output!')

    for key in dir(model):
        if key[0] == "_":
            continue
        value = getattr(model, key)
        print(key, value.shape, np.max(value), np.min(value))

    return model


def model_to_struct(model):
    faces = model.faces - 1
    nverts = int(len(model.shapeMU)/3)

    struct = {
        "expEV" : model.expEV,
        "expMU" : np.reshape(model.expMU, (nverts,3)),
        "expPC" : np.reshape(model.expPC, (nverts,3,len(model.expEV))),

        "shapeEV" : model.shapeEV,
        "shapeMU" : np.reshape(model.shapeMU, (nverts,3)),
        "shapePC" : np.reshape(model.shapePC, (nverts,3,len(model.shapeEV))),

        "texEV" : model.texEV,
        "texMU" : np.reshape(model.texMU, (nverts,3)),
        "texPC" : np.reshape(model.texPC, (nverts,3,len(model.texEV))),

        "faces" : [[int(vn) for vn in vnums] for vnums in faces],
        "innerLandmarkIndex" : [int(vn) for vn in model.innerLandmarkIndex],
        "outerLandmarkIndex" : [int(vn) for vn in model.outerLandmarkIndex],
    }
    return struct


def save_bfm():
    from common import save_json
    model = get_bfm()
    struct = model_to_struct(model)

    jstruct = {
        "expEV" : list(struct["expEV"]),
        "expMU" : list2(struct["expMU"]),
        "expPC" : list3(struct["expPC"], 5),
        "shapeEV" : list(struct["shapeEV"]),
        "shapeMU" : list2(struct["shapeMU"]),
        "shapePC" : list3(struct["shapePC"], 5),
        "texEV" : list(struct["texEV"]),
        "texMU" : list2(struct["texMU"]),
        "texPC" : list3(struct["texPC"], 5),
        "faces" : struct["faces"],
        "innerLandmarkIndex" : struct["innerLandmarkIndex"],
        "outerLandmarkIndex" : struct["outerLandmarkIndex"],
    }
    print("Dump bfm.json")
    save_json(jstruct, "../data/bfm.json", use_binary=True)


def list2(arr):
    return [[float(x) for x in co] for co in arr]


def list3(arr, nmax):
    print("list3", arr.shape)
    morphs = []
    for n in range(arr.shape[2]):
        morph = [[float(x) for x in co] for co in arr[:,:,n]]
        morphs.append(morph)
    return morphs


def project_bfm(model, features, expr_paras, pose_paras, tex_params):
    # Shape
    alpha = model.shapeEV * 0
    for it in range(0, 99):
        alpha[it] = model.shapeEV[it] * features[it]
    S = np.matmul(model.shapePC, alpha)
    S = model.shapeMU + S
    numVert = int(S.shape[0]/3)

    # Expression
    if expr_paras:
        expr = model.expEV * 0
        for it in range(0, 29):
            expr[it] = model.expEV[it] * expr_paras[it]
        E = np.matmul(model.expPC, expr)
        S = S + model.expMU + E

    # Pose
    if pose_paras:
        from cv2 import Rodrigues
        r = pose_paras[0:3]
        r[1] = -r[1]
        r[2] = -r[2]
        t = pose_paras[3:6]
        t[0] = -t[0]
        R, jacobian = Rodrigues(r, None)
        S_RT = np.matmul(R, np.transpose(S)) + np.reshape(t, [3,1])
        S = np.transpose(S_RT)

    S = np.reshape(S,(numVert,3))
    T = None

    # (Texture)
    if tex_params:
        beta = model.texEV * 0
        print(len(beta), len(model.texEV), len(tex_params))
        for it in range(0, 99):
            beta[it] = model.texEV[it] * tex_params[it]
        T = np.matmul(model.texPC, beta)
        T = model.texMU + T
        T = [truncateUint8(value) for value in T]
        T = np.reshape(T,(numVert, 3))

    return S,T


def truncateUint8(val):
    return max(0, min(255, int(val)))


def write_ply(path, S, T, faces):
    nV = S.shape[0]
    nF = faces.shape[0]
    with open(path,'w') as fp:
        fp.write('ply\n')
        fp.write('format ascii 1.0\n')
        fp.write('element vertex ' + str(nV) + '\n')
        fp.write('property float x\n')
        fp.write('property float y\n')
        fp.write('property float z\n')
        if T is not None:
            fp.write('property uchar red\n')
            fp.write('property uchar green\n')
            fp.write('property uchar blue\n')
        fp.write('element face ' + str(nF) + '\n')
        fp.write('property list uchar int vertex_indices\n')
        fp.write('end_header\n')

        if T is not None:
            for i in range(0,nV):
                fp.write('%0.4f %0.4f %0.4f %d %d %d\n' % (S[i,0],-S[i,2],S[i,1],T[i,0],T[i,1],T[i,2]))
        else:
            for i in range(0,nV):
                fp.write('%0.4f %0.4f %0.4f\n' % (S[i,0],-S[i,2],S[i,1]))

        for i in range(0,nF):
            fp.write('3 %d %d %d\n' % (faces[i,0],faces[i,1],faces[i,2]))


def save_shapes():
    model = get_bfm()
    struct = {}
    for n in range(99):
        shape = np.zeros(99)
        shape[n] = 1
        #S,T = project_bfm(model, struct["shape"], struct["expr"], struct["pose"], struct["texture"])
        S,T = project_bfm(model, shape, None, None, None)
        struct["%02d" % n] = [[float(x) for x in co] for co in S]
        print(n)

    common.dump_json(struct, "../data/shapes.json")
    print("Done")


def save_ply(name):
    config = common.load_config()

    if name is None:
        shape = np.zeros(99)
        name = "BFM"
    elif name.isdigit():
        shape = np.zeros(99)
        n = int(name)
        shape[n] = 10
        name = ("%02d" % n)
    else:
        params = common.get_folder([config["autoface_dir"], "parameters/"])
        inpath = common.get_abs_path([params, name + "_params.json"])
        struct = common.load_json(inpath)
        shape = struct["shape"]

    bfm = common.get_folder(config["autoface_dir"], "bfm/")
    outpath = common.get_abs_path([bfm, name + ".ply"])

    model = get_bfm()
    S,T = project_bfm(model, shape, None, None, None)
    S = 0.001 * S
    write_ply(outpath, S, T, model.faces-1)
    print(outpath, "written")


if __name__ == "__main__":
    save_bfm()
    quit()

    if len(sys.argv) < 2:
        name = None
    else:
        name = sys.argv[1]
    save_ply(name)