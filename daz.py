# Copyright (c) 2019, Thomas Larsson
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
# ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
# The views and conclusions contained in the software and documentation are those
# of the authors and should not be interpreted as representing official policies,
# either expressed or implied, of the FreeBSD Project.

import bpy
import os
import gzip
from mathutils import Vector
from .error import AutoFaceError
from .standalone.common import load_json, load_config, join_paths, get_abs_path


def encode(path):
    return path.replace(" ", "%20")


def get_morph_path(config):
    return join_paths([config["morph_dir"], "FHM-" + config["name"]+".dsf"])

def get_full_character_path(config):
    return get_abs_path([config["output_base_dir"], config["name"]+".dsf"])

def get_people_path(config):
    return join_paths([config["people_dir"], "Characters", config["name"]+".duf"])

def get_texture_path(config):
    return join_paths([config["tex_dir"], config["diffuse_texture"]])

def get_full_texture_path(config):
    return get_abs_path([config["output_base_dir"], config["output_tex_dir"], config["name"]+"_diffuse.png"])

def get_parent_path(config):
    return get_abs_path([config["daz_base_dir"], config["people_dir"], config["daz_base"]])


class VIEW3D_OT_SaveDazFiles(bpy.types.Operator):
    bl_idname = "aufc.save_daz_files"
    bl_label = "Save DAZ Files"
    bl_description = "Save the current character for use in Daz Studio."
    bl_options = {'UNDO'}

    @classmethod
    def poll(self, context):
        ob = context.object
        return (ob and ob.type == 'MESH' and ob.data.shape_keys)

    def execute(self, context):
        try:
            self.run(context)
        except AutoFaceError:
            pass
        return{'FINISHED'}

    def run(self, context):
        from .match import get_base_mesh
        ob = context.object
        self.use_binary = context.scene.AufcBinary
        config = load_config(ob=ob, base=get_base_mesh(ob))
        self.character = config["id"]
        icon = self.copy_icon(ob, config)
        texture = self.copy_texture(ob, config)
        tip = self.copy_tip(ob, config)
        self.make_morph_file(ob, ob.parent, icon, config)
        self.make_character_file(ob, icon, texture, config)


    def copy_icon(self, ob, config):
        png = os.path.splitext(ob.AufcImgfile)[0] + ".png"
        src = get_abs_path([config["autoface_dir"], "icons/", png])
        trg = join_paths([config["morph_dir"], png])
        self.copy_image(src, trg, config)
        return trg


    def copy_tip(self, ob, config):
        fname = os.path.splitext(ob.AufcImgfile)[0]
        png = fname + ".png"
        src = get_abs_path([config["autoface_dir"], "icons/", png])
        folder = os.path.dirname(get_people_path(config))
        trg = join_paths([folder, png])
        self.copy_image(src, trg, config)
        trg = join_paths([folder, fname + ".tip.png"])
        self.copy_image(src, trg, config)
        return trg


    def copy_texture(self, ob, config):
        texname = os.path.splitext(ob.AufcImgfile)[0] + "_diffuse.png"
        src = get_abs_path([config["autoface_dir"], "textures/", texname])
        trg = join_paths([config["output_tex_dir"], texname])
        self.copy_image(src, trg, config)
        return trg


    def copy_image(self, src, trg, config):
        from shutil import copyfile
        if not os.path.exists(src):
            print("Image file '%s' does not exist." % src)
            return ""
        trg = get_abs_path([config["output_base_dir"], trg])
        if src != trg:
            folder = os.path.dirname(trg)
            if not os.path.exists(folder):
                print("Making folder %s" % folder)
                os.makedirs(folder)
            print("Copy %s\n => %s" % (src, trg))
            copyfile(src, trg)


    def make_morph_file(self, ob, rig, icon, config):
        from .match import get_base_mesh
        path = get_morph_path(config)
        struct = load_json("./dsf/Sample Head.dsf", ordered=True)
        struct["asset_info"]["id"] = path

        self.fhm_id = ("FHM-%s" % config["name"])
        self.fhm_parent = encode(config["parent"])
        modifier = struct["modifier_library"][0]
        modifier["id"] = self.fhm_id
        modifier["name"] = self.fhm_id
        modifier["parent"] = ("%s#geometry" % self.fhm_parent)
        modifier["presentation"]["icon_large"] = encode(icon)
        modifier["channel"]["label"] = config["name"]

        # formulas
        self.formulas = modifier["formulas"] = []
        if rig:
            for bone in rig.data.bones:
                self.add_formulas("center_point", bone.name, bone.AufcHOffset)
                self.add_formulas("end_point", bone.name, bone.AufcTOffset)

        # morph
        coords = load_final_coords(ob)
        deltas = []
        for vn,delta in coords.items():
            x,y,z = delta
            deltas.append([vn, 100*x, 100*z, -100*y])
        deltas.sort()
        morph = modifier["morph"]
        morph["vertex_count"] = len(ob.data.vertices)
        morph["deltas"]["count"] = len(deltas)
        morph["deltas"]["values"] = deltas

        # scene
        scn_modifier = struct["scene"]["modifiers"][0]
        scn_modifier["id"] = ("FHM-%s-1" % config["name"])
        scn_modifier["url"] = encode("#%s" % self.fhm_id)

        path = get_abs_path([config["output_base_dir"], get_morph_path(config)])
        save_json_pretty(struct, path, self.use_binary)
        print("Saved", os.path.realpath(path))


    def add_formulas(self, channel, bname, offset):
        x,y,z = offset
        self.add_formula(channel + "/x", bname, x)
        self.add_formula(channel + "/y", bname, z)
        self.add_formula(channel + "/z", bname, -y)


    def add_formula(self, channel, bname, x):
        if abs(x) > 1e-5:
            formula = {
                "output" : "%s:%s#%s?%s" % (bname, self.fhm_parent, bname, channel),
                "operations" : [
                    { "op" : "push", "url" : "%s:#%s?value" % (self.character, encode(self.fhm_id)) },
                    { "op" : "push", "val" : 100*x },
                    { "op" : "mult" }
                ]
            }
            self.formulas.append(formula)


    def make_character_file(self, ob, icon, newdiffuse_path, config):
        parent = get_parent_path(config)
        struct = load_json(parent, ordered=True)

        path = get_people_path(config)
        ainfo = struct["asset_info"]
        ainfo["id"] = path
        ainfo["contributor"]["author"] = "AutoFace"

        # image library
        diffuse_path = get_texture_path(config)
        diffuse = os.path.basename(diffuse_path)
        newdiffuse = os.path.basename(newdiffuse_path)

        for image in struct["image_library"]:
            if diffuse in image["id"]:
                image["id"] = image["id"].replace(diffuse, newdiffuse)
                image["name"] = image["name"].replace(diffuse, newdiffuse)
                for map in image["map"]:
                    map["url"] = newdiffuse_path
                    map["label"] = newdiffuse

        scene = struct["scene"]

        for mat in scene["materials"]:
            if "diffuse" in mat.keys():
                if "channel" in mat["diffuse"].keys():
                    channel = mat["diffuse"]["channel"]
                    if "image_file" in channel.keys():
                        if channel["image_file"] == diffuse_path:
                            channel["image_file"] = newdiffuse_path

        for node in scene["nodes"]:
            if node["id"] == config["id"]:
                node["label"] = config["name"]

        morph_path = get_morph_path(config)
        mod = {
            "id" : ("FHM-%s" % config["name"]),
            "url" : encode(morph_path + ("#FHM-%s" % config["name"])),
            "parent" : ("#%s" % config["id"]),
            "channel" : {
                "id" : "value",
                "type" : "float",
                "name" : "Value",
                "value" : 1,
                "current_value" : 1
            }
        }
        scene["modifiers"] = [mod] + scene["modifiers"]

        path = get_abs_path([config["output_base_dir"], get_people_path(config)])
        save_json_pretty(struct, path, self.use_binary)
        print("Saved", os.path.realpath(path))


def load_final_coords(ob):
    from .match import get_base_mesh
    base = get_base_mesh(ob)
    parts = load_json("./meshes/%s-match.json" % base)
    vnums = [vn for _,vn in parts["face"]]
    parts = load_json("./meshes/%s-faceparts.json" % base)
    vnums += parts["left eye"] + parts["right eye"] + parts["upper jaw"] + parts["lower jaw"]

    coords = dict([(vn,Vector((0,0,0))) for vn in vnums])
    verts = ob.data.vertices
    nskeys = 0
    for skey in ob.data.shape_keys.key_blocks:
        if skey.name == "Basic":
            continue
        for vn in coords.keys():
            coords[vn] += (skey.data[vn].co - verts[vn].co) * skey.value
            nskeys += 1
    return coords


def save_json_pretty(struct, filepath, use_binary=False):
    folder = os.path.dirname(filepath)
    if not os.path.exists(folder):
        print("Make folder: %s" % folder)
        os.makedirs(folder)

    string = encode_json_data(struct, "") + "\n"
    if use_binary:
        bytes = string.encode(encoding="utf-8")
        with gzip.open(filepath, 'wb') as fp:
            fp.write(bytes)
    else:
        import codecs
        with codecs.open(filepath, "w", encoding="utf-8") as fp:
            fp.write(string)


def encode_json_data(data, pad=""):
    if data is None:
        return "null"
    elif isinstance(data, (bool)):
        if data:
            return "true"
        else:
            return "false"
    elif isinstance(data, (float)):
        if abs(data) < 1e-6:
            return "0"
        else:
            return "%.5g" % data
    elif isinstance(data, (int)):
        return str(data)

    elif isinstance(data, (str)):
        return "\"%s\"" % data
    elif isinstance(data, (list, tuple, Vector)):
        if leaf_list(data):
            string = "["
            string += ",".join([encode_json_data(elt) for elt in data])
            return string + "]"
        else:
            string = "["
            string += ",".join(
                ["\n    " + pad + encode_json_data(elt, pad+"    ")
                 for elt in data])
            if string == "[":
                return "[]"
            else:
                return string + "\n%s]" % pad
    elif isinstance(data, dict):
        string = "{"
        string += ",".join(
            ["\n    %s\"%s\" : " % (pad, key) + encode_json_data(value, pad+"    ")
             for key,value in data.items()])
        if string == "{":
            return "{}"
        else:
            return string + "\n%s}" % pad
    else:
        try:
            string = "["
            string += ",".join([encode_json_data(elt) for elt in data])
            return string + "]"
        except:
            print(data)
            print(data.type)
            raise AutoFaceError("Can't encode: %s %s" % (data, data.type))


def leaf_list(data):
    for elt in data:
        if isinstance(elt, (list,dict)):
            return False
    return True