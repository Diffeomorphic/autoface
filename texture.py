# Copyright (c) 2019, Thomas Larsson
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
# ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
# The views and conclusions contained in the software and documentation are those
# of the authors and should not be interpreted as representing official policies,
# either expressed or implied, of the FreeBSD Project.

import bpy
import os
from bpy_extras.io_utils import ImportHelper, ExportHelper
from bpy.props import StringProperty
from .error import AutoFaceError
from .standalone.common import get_abs_path, join_paths, load_config


class VIEW3D_OT_LoadTexture(bpy.types.Operator, ImportHelper):
    bl_idname = "aufc.load_texture"
    bl_label = "Load Texture"
    bl_description = "Load new texture to the diffuse channel of the face and lips materials"
    bl_options = {'UNDO'}

    filename_ext = ".png;.jpeg;.jpg;.bmp"
    filter_glob = StringProperty(default="*.png;*.jpeg;*.jpg;*.bmp", options={'HIDDEN'})
    filepath = StringProperty(
        name="File Path",
        maxlen=1024,
        default="")

    @classmethod
    def poll(self, context):
        return (context.object and context.object.type == 'MESH')

    def execute(self, context):
        try:
            self.run(context.object)
        except AutoFaceError:
            pass
        return{'FINISHED'}

    def run(self, ob):
        img = bpy.data.images.load(self.filepath)
        img.name = os.path.splitext(os.path.basename(self.filepath))[0]
        for mname in ["Face-1", "Lips-1"]:
            for mat in ob.data.materials:
                if mname in mat.name:
                    self.replace_diffuse(mat, img)


    def invoke(self, context, event):
        config = load_config(ob=context.object)
        self.properties.filepath = get_abs_path([config["autoface_dir"], "textures/"], isdir=True)
        context.window_manager.fileselect_add(self)
        return {'RUNNING_MODAL'}


    def replace_diffuse(self, mat, img):
        if mat.node_tree:
            for link in mat.node_tree.links:
                if (link.to_node.type == "BSDF_PRINCIPLED" and
                    link.to_socket.name == "Base Color"):
                    self.replace_texture_node(link.from_node, mat, img)
                elif (link.to_node.type == "BSDF_DIFFUSE" and
                    link.to_socket.name == "Color"):
                    self.replace_texture_node(link.from_node, mat, img)
        else:
            for mtex in mat.texture_slots:
                if mtex and mtex.use_map_color_diffuse and mtex.texture:
                    tex = mtex.texture
                    if tex.type == 'IMAGE':
                        tex.image = img
                        tex.name = img.name


    def replace_texture_node(self, node, mat, img):
        if node is None:
            return
        elif node.type == "TEX_IMAGE":
            node.image = img
        elif node.type == "MIX_RGB":
            for link in mat.node_tree.links:
                if (link.to_node == node and
                    link.to_socket.name != "Fac"):
                    self.replace_texture_node(link.from_node, mat, img)


class VIEW3D_OT_SaveTexture(bpy.types.Operator, ExportHelper):
    bl_idname = "aufc.save_texture"
    bl_label = "Save Texture"
    bl_description = "Save face diffuse texture"
    bl_options = {'UNDO'}

    filename_ext = ""
    filter_glob = StringProperty(default="*.png;*.jpeg;*.jpg;*.bmp", options={'HIDDEN'})
    filepath = StringProperty(
        name="File Path",
        maxlen=1024,
        default="")

    @classmethod
    def poll(self, context):
        return (context.object and context.object.type == 'MESH')

    def execute(self, context):
        try:
            self.run(context.object)
        except AutoFaceError:
            pass
        return{'FINISHED'}


    def invoke(self, context, event):
        config = load_config(ob=context.object)
        self.properties.filepath = get_abs_path([config["autoface_dir"], "textures/"], isdir=True)
        context.window_manager.fileselect_add(self)
        return {'RUNNING_MODAL'}


    def run(self, ob):
        from .match import get_base_mesh
        from shutil import copyfile

        for mname in ["Face-1", "Lips-1"]:
            for mat in ob.data.materials:
                if mname in mat.name:
                    img = self.get_diffuse(mat)
                    if img:
                        break

        if img is None:
            raise AutoFaceError("No face image found")

        src = bpy.path.abspath(img.filepath)
        src = bpy.path.reduce_dirs([src])[0]
        trg = os.path.splitext(self.filepath)[0] + os.path.splitext(src)[1]
        if src != trg:
            print("Copy %s\n => %s" % (src, trg))
            copyfile(src, trg)


    def get_diffuse(self, mat):
        if mat.node_tree:
            for link in mat.node_tree.links:
                if (link.to_node.type == "BSDF_PRINCIPLED" and
                    link.to_socket.name == "Base Color"):
                    return self.get_texture_node(link.from_node, mat)
                elif (link.to_node.type == "BSDF_DIFFUSE" and
                    link.to_socket.name == "Color"):
                    return self.get_texture_node(link.from_node, mat)
        else:
            for mtex in mat.texture_slots:
                if mtex and mtex.use_map_color_diffuse and mtex.texture:
                    tex = mtex.texture
                    if tex.type == 'IMAGE':
                        return tex.image


    def get_texture_node(self, node, mat):
        if node is None:
            return None
        elif node.type == "TEX_IMAGE":
            return node.image
        elif node.type == "MIX_RGB":
            for link in mat.node_tree.links:
                if (link.to_node == node and
                    link.to_socket.name != "Fac"):
                    return self.get_texture_node(link.from_node, mat)

