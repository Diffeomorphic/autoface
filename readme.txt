Prerequisites

1. Download DAZ Studio from https://www.daz3d.com/.

DAZ Studio is free but you need to register to download it. It comes with some free assets, including the Genesis 8 Male and Female characters that the Autoface add-on uses as base meshes. 


2. Download Blender 2.79b from https://www.blender.org/.

Autoface is an add-on for Blender 2.79. It does not run on Blender 2.8x.


3. Download the DAZ importer from http://diffeomorphic.blogspot.com/p/daz-importer-version-13.html.

The DAZ importer is a Blender add-on that imports native DAZ Studio files (.duf, .dsf) into Blender. To install the add-on, follow the instructions at http://diffeomorphic.blogspot.com/p/installing-blender-add-on.html.


4. Download Autoface.

Repository: https://bitbucket.org/Diffeomorphic/autoface.
Zip file: https://bitbucket.org/Diffeomorphic/autoface/downloads/

Install the Autoface add-on in the same way as the DAZ importer. This add-on uses some data files that are too large to put in a Bitbucket repository. Download this data from:

https://www.dropbox.com/s/mf0pxjiryr9806z/autoface_data.zip

Put this file in the Autoface directory and unzip it. This will create a folder named data.


5. Install OpenCV following the instructions at https://www.learnopencv.com/?s=install.

Windows: https://www.learnopencv.com/install-opencv-4-on-windows/
Mac: https://www.learnopencv.com/install-opencv-4-on-macos/

On Windows you start by installing the following software:
	5.1. Visual Studio.
	5.2. CMake.
	5.3. Anaconda. I installed the Python 3 version but Python 2 will probably work too.
	5.4. Git for Windows.

The instructions refer to install scripts whose location is not immediately obvious. The Windows versions can be found at https://github.com/spmallick/learnopencv/tree/master/InstallScripts/Windows-4.


6. Install dlib.

Official instructions: http://dlib.net/compile.html.
Github: https://github.com/davisking/dlib.

Grab the latest sources from github, go to the base folder of the dlib repository, and run 
> python setup.py install


7. Install tensorflow.

Well, I failed to do that, so I am not the one to give instructions. Instead, we will run tensorflow on Google Colaboratory, or Colab for short. Many useful packages such as OpenCV, Dlib and tensorflow are preinstalled on Colab.


8. Get a Google account and Google drive.

https://www.google.com/drive/

They give you 15 Gb storage for free, which is more than enough to run the Autoface script.


9. Open the autoface notebook on Colab.

Link: https://colab.research.google.com/drive/1p_6MQhzvAs2Uiiy0UQUiK-oUH2B3NIju


10. Sign in to Colab with your Google account and save a copy of the notebook. Make sure to save it as a Python 2 notebook. 


11. Mount your Google drive by executing the first cell.

from google.colab import drive
drive.mount('/content/gdrive')

To run the cell, press on the arrow to the left of it. By clicking on the link you will get a long password that should be pasted into the box.


12. Install Expression-Net by running the next cell.

%cd "/content/gdrive/My Drive/"
!git clone https://github.com/fengju514/Expression-Net.git
%cd Expression-Net

!cp ./kaffe/tensorflow/network_shape.py ./ResNet/
!cp ./kaffe/tensorflow/network_expr.py ./ResNet/

For more information about Expression-Net, see https://github.com/fengju514/Expression-Net.git


13. Download the following learned deep models:

ExpressionNet from https://www.dropbox.com/s/frq7u7z5kgxnz9e/Expression_Model.tar.gz?dl=0
Identity shape model from https://www.dropbox.com/s/ej80o9lnj0k49qu/Shape_Model.tar.gz?dl=0, and
FacePoseNet from https://www.dropbox.com/s/r38psbq55y2yj4f/fpn_new_model.tar.gz?dl=0. 

Make sure that the ExpressionNet, Shape, and FacePoseNet models are stored in the folders Expression_Model, Shape_Model, and fpn_new_model respectively.


14. The Autoface add-on, which was downloaded in step 4, contains a directory named exprnet. Copy the content of this directory into the Expression-Net directory on Google Drive. The latter should have been created by the Colab notebook in step 12.

The exprnet directory contains the python file autoface.py, a data file input.csv, and a folder with example images.


Running Autoface

1. The Autoface add-on 


2. Open a terminal window and go to the standalone directory.


3. Create landmarks and icons with the python script. From the command prompt, run:

python landmarks.py <input-image> <basemesh>

The input image is assumed to be located in the source folder specified in config.txt. The basemesh is either g8m or g8f, for Genesis 8 Male and Genesis 8 Female, respectively. E.g.,

python landmarks.py donald_trump.jpg g8m
python landmarks.py hillary_clinton.jpg g8f

After the script has completed, the image is shown with facial landmarks. Four folders are created in the Documents/Autoface directory:

figures: Images and csv files, which can be uploaded to Google drive for analysis.
icons: Thumbnail versions of the images to be used in DAZ Studio (currently empty).
parameters: 
results: Information about the picture to be used in the next step below.


4. Activate OpenCV. From the command prompt, run:

activate OpenCV-master-py3


5. Patch the image into the diffuse texture. From the command prompt, run:

python patch.py <input-image> <basemesh>

E.g.

python patch.py donald_trump.jpg g8m
python patch.py hillary_clinton.jpg g8f

This creates the diffuse face texture and puts it in the location where DAZ Studio can find it.


6. Upload the figures directory to the Expression-Net folder on Google drive. 


7. In Colab, go to the Expression-Net folder.

%cd "/content/gdrive/My Drive/Expression-Net"


8. Analyze the images in Colab.

!python analyze.py <csv-file>

E.g.

!python autoface.py ./figures/donald_trump.csv
!python autoface.py ./figures/hillary_clinton.csv

Alternatively, you can analyze several images in a batch:

!python autoface.py ./input.csv


9. The script create json files in the output_parameters directory. The 


